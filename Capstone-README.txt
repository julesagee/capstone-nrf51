Capstone NRF51 readme

Download the NRF51 SDK version 11.0.0 and put this source
tree in the examples\ble_peripheral folder.

Then in Keil uVision5, open this project file:

examples\ble_peripheral\ble_ecg_project\pca10028\s130\arm5_no_packs\nrf51-ble-tutorial-characteristic

Note you might need to use nRFGo Studio to load the s130 softdevice on your
NRF51 processor before this will work. The s130 softdevice is located in this
directory from the nRF51 SDK 11.0.0:

components\softdevice\s130\hex